\section{Evaluation}
\label{evaluation}
\subsection{Dataset Description}
In our evaluation, we perform a large scale trace-driven simulation using real-world taxi trajectory dataset. The dataset contains more than 81 million GPS locations of over 3,000 taxis during a period of three month, from October 2013 to December 2013, in Zhenjiang (a city in Jiangsu, China).  The dataset has the following key attributes: GPS coordinates and taxi status (occupied or not). Based upon the status information, we generate passenger requests -- a request's pick-up and drop-off locations are the beginning and end locations of an occupied period. In total,  we generated 1.8 million requests.

\subsection{Evaluation Methodology}
\subsubsection{Taxi-sharing Schemes}  we compare four dynamic taxi-sharing algorithms: 1) the optimal integer linear programming algorithm, referred to as \textbf{Optimal-Share}; 2) the heuristic algorithm, referred to as \textbf{QA-Share}; 3)\textbf{T-Share} discussed in~\cite{DBLP:conf/icde/MaZW13}; and 4) a completely distributed taxi sharing service without a centralized dispatching server, referred to as \textbf{Distri-Share}, in which a random taxicab near the request is going to serve the request. In our evaluation, the ground truth is the original GPS traces of the dataset.

\subsubsection{Simulation Initialization} To make simulations more realistic, we use the real taxi status data in our simulations. Suppose we start simulations
from a certain timestamp $t_s$ (the simulated time). We then use each taxicab's information in the trace before $t_s$ as the initial status of that taxi. Specifically,
 we start each single simulation at 7AM and end at  11PM.

\begin{figure}[t]
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/profit_delta}
% \vspace{-10em}
\caption{Profit vs. delta in busy hours}
\label{LargeScale}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/profit_delta_small}
\caption{Profit vs. delta in leisure hours}
\label{SmallScale}
\end{minipage}
% \vspace{-1.5em}
\end{figure}

\subsubsection{Simulation Scale} Note that the taxi GPS dataset only includes requests that got served, while many requests were unserved in real life due to failure to get a taxi. The motivation of a taxi-sharing system is to serve these requests that can't be served in traditional taxi system. In order to accommodate the unserved requests in the simulation, we introduce a parameter $\delta$ to simulate more requests than available in the dataset --  in our simulations, we combine the requests during $\delta$ days in the dataset as the requests simulated in one day. In the dataset, the number of requests in a single day is about 20,000 on average, and we have $\delta = 4$ unless otherwise specified.

\subsubsection{Request Attributes} Note that we can only extract $R.p$ and $R.d$ from the dataset. We set the earliest pick-up time $R.pw.e$ as the arrival time of the request, the latest pick-up time $R.pw.l$ as $R.pw.e + DT$ where $DT$ is a tolerable detour time; the delivery deadline $R.dt$ as the sum of $R.pw.l$ and the average travel time between the pick-up and drop-off locations; the number of passengers $R.n$ as $1$; the tip as a linear decay function, where $R.t.\alpha$ is a random number between $0.5$ dollar and $10$ dollars; and $R.t.\beta$ calculated by $R.t.\alpha/DT$, where $DT$ is 15 minutes unless otherwise specified.

\subsubsection{System parameters} We have the following three system parameters: process time window (or schedule update interval) $PW$ (Sec.~\ref{overviewServer}),  schedule update threshold $\Phi$ (Sec.~\ref{updatemechanismsection}),  and the threshold $\tau$ that is used to determine whether to use the optimal ILP scheduling or the heuristic approach. Unless otherwise specified, we have $PW = 3 min, \Phi = 2, \tau = 4$. % if not specified. We mainly simulate in the busy hours (7 a.m. to 11 p.m.) of a day. 2396 taxis operate in the busy in total.

\subsection{Simulation Results}
%The performance of our taxi-sharing service is evaluated by the following measures.

\textbf{Profit:} We compare the increased profit (from non-sharing taxi system) by varying the value of $\delta$ with the tolerated detour time ($DT$) of 15 minutes. We used the dataset in busy hours to evaluate $QA-Share$, and the dataset in leisure hours (i.e., 1AM to 5AM) to evaluate $Optimal-Share$ (237 taxicabs). %We also vary the value of $DT$.


Fig.~\ref{LargeScale} reports the percentage of increased profit of QA-share, T-share, and Distri-Share with increasing request rate. We have the following observations.  First, as the number of requests increases, the percentage of increased profit for all three schemes also increases, demonstrating that taxi-sharing is a viable solution compared to traditional non-sharing system.  Second, we find that it is beneficial to have a centralized dispatching server in taxi-sharing. Third, QA-Share results in higher profit than T-Share, and the improvement can be as high as 16\%.  Fig.~\ref{SmallScale} evaluates different taxi-sharing schemes in leisure hours, and we observe that QA-Share is close to Optimal-Share, which is the best among all the schemes.

\begin{figure}[t]
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/profit_DT}
\caption{Profit vs. DT in busy hours}
\label{DTLargeScale}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/profit_DT_small}
\caption{Profit vs. DT in leisure hours}
\label{DTSmallScale}
\end{minipage}
% \vspace{-2em}
\end{figure}
Fig.~\ref{DTLargeScale} shows the percentage of increased profit of different sharing schemes with different $DT$ values during busy hours. Here, we observe the same trend as in Fig.~\ref{LargeScale}, with QA-Share leading the performance. Fig.~\ref{DTSmallScale} compares the sharing schemes in leisure hours while varying the $DT$ value. Again, we observe the same trend as in Fig.~\ref{SmallScale}.

\textbf{Request Service Rate} is the fraction of requests that were served in the simulations. We first compare the service rates by varying the value of  $\delta$ during the busy hours, and summarize the results in  Fig.~\ref{satisfaction_delta}.  We observe that as the number of requests increases, the service rate decreases, and QA-share performs better than T-share.  We also compare the service rate by varying the passengers' tolerable detour time (with $\delta = 4$) and show the results in Fig.~\ref{satisfaction_DT}. As expected, the service rate increases when the value of $DT$ increases. In particular, QA-share performs much better than T-share at larger $DT$ values. This is because with larger $DT$ values,  the requests are more likely to be rescheduled for better performance.


\begin{figure}[t]
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/satisfaction_delta}
\caption{Request service rate vs. delta}
\label{satisfaction_delta}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/satisfaction_DT}
\caption{Request service rate vs. DT}
\label{satisfaction_DT}
\end{minipage}
\end{figure}

\textbf{Extra Travel Distance:} We use $D_S$ to denote the total distance traveled by the taxis with taxi sharing, and $D_{SR}$ the total distance without taxi sharing. Usually we have $D_{SR} > D_S$. The effective distance ratio,  $D_S/D_{SR}$, measures the efficiency of the taxi-sharing system.   Fig.~\ref{distanceratio_delta} shows the effective distance ratio when varying the value of $\delta$. Results show that when the number of requests increases, the effective distance ratio for the sharing schemes decreases, since the system travels more to serve the requests. Also, since T-share aims to minimize travel distance, it performs better than QA-Share, at a cost of serving fewer requests and gaining less profit.  %***YZ: this is not true! This metric is bigger the better, and QA-Share has a larger ratio!!! ***

We also study the effective distance ratio when we vary the passengers' tolerated detour time, $DT$. We have $\delta = 4$ in this set of experiments, and show the results in Fig.~\ref{distanceratio_DT}. As expected, the effective distance ratio decreases with the increase of $DT$.
\begin{figure}[t]
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/distanceratio_delta}
\caption{Distance ratio vs. delta}
\label{distanceratio_delta}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/distanceratio_DT}
\caption{Distance ratio vs. DT}
\label{distanceratio_DT}
\end{minipage}
% \vspace{-2em}
\end{figure}


\subsection{Evaluation of system parameters}
\textbf{Process Window($PW$):} QA-share re-calculates the schedule every $PW$ time. We compare the profit gain by varying the value of $PW$. We set $DT=15~mins$ in this set of experiments. Fig.~\ref{profit_processwindow} shows the simulation results. We find that when $PW$ is larger than 5 minutes, or smaller than 1 minute, QA-Share does not fare well in profit gain.  This is because large process windows lead to delayed processing of the requests;  a small process window only leads to a small number of requests to be considered, hence less performance gain.

\textbf{Algorithm threshold($\tau$):} This threshold is used to decide whether to use QA-Share or Optimal-Share when calculating the schedule. We seek to find a suitable value for $\tau$ by observing the execution time of Optimal-Share. Fig.~\ref{runningtime} shows the results. We observe that the execution time of Optimal-Share increases considerably when the number of requests is larger than 12. In this case, we can set $\tau=12$.

\begin{figure}[t]
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\linewidth]{fig/profit_processwindow}
\caption{{Profit vs. Process Window}}
\label{profit_processwindow}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[b]{0.48\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/runtime_requestnumber}
\caption{Running time comparison}
\label{runningtime}
\end{minipage}
\end{figure}


\textbf{Update threshold($\Phi$):} We update the schedules if the new schedule has a profit increase larger than $\Phi$. Here, we evaluate the impact of $\Phi$ on profit gain, and the degree of oscillations. An oscillation is defined as a taxi travel to the same location three times in 5 minutes. %***YZ: Why 3 times per 5 minutes??***
Fig. 16 shows the simulation results. With the increase of $\Phi$, the percentage of oscillation decreases. The profit gain reaches the maximum when we have $\Phi=1.5$. Low update thresholds result in oscillations, while high update thresholds lead to little change to the schedule. We note that even though an update threshold at 1.5 leads to maximum profit, it also results in $3.2\%$ of oscillations. In order to experience a lower oscillation likelihood, we recommend a slightly higher threshold value, e.g. 2.



\begin{figure}[t]
\label{updatethresholdexp}
\subfigure{
\includegraphics[width=.46\linewidth]{fig/updatethreshold_profit}}
\hspace{0.2cm}
\subfigure{
\includegraphics[width=.46\linewidth]{fig/updatethreshold_osci}}
\caption{Influence of update threshold}
% \vspace{-2em}
\end{figure}
% \begin{figure}[!t]
% \label{updatethreshold}
% \subfigure[]{
% \includegraphics[width=.4\linewidth]{fig/updatethreshold_profit}}

% \subfigure[]{
% \includegraphics[width=.4\linewidth]{fig/updatethreshold_osci}}
% \caption{Influence of Update Threshold}
% \end{figure}