\section{Pre-processing}
In the pre-processing stage, we first represent a 2 dimensional city area using a graph $G(N,E)$. We then design a geographic clustering algorithm to divide the graph $G(N,E)$ into $k$ regions, which facilitates fast estimation of travel time between two locations, as well as index construction in the online stage.


\subsection{Map Decomposition}
As shown in Fig.~\ref{cluster_illustration}, we first discretize the terrain of a city, which is a continuous 2-D area, into a graph $G(N,E)$ (a.k.a. digital map), where $N$ represents locations with latitude/longitude coordinates, and $E$ represents paths between any two locations~\cite{biagioni2012inferring}. For each edge, we also estimate the travel time of the corresponding road segment.%Please note that we may represent a long street using multiple edges.
%Quite a few studies (e.g.,~\cite{biagioni2012inferring}) looked at constructing digital maps from vehicle trajectories.
Several studies\cite{DBLP:conf/aaai/ZhengN13,DBLP:conf/aaai/IdeS11} looked at estimating traffic conditions on the edges in $G$. In this paper, we use the regression algorithm proposed in~\cite{DBLP:conf/aaai/IdeS11} to estimate a static transition speed on each edge. We can easily extend our work to a dynamic traffic scenario by considering the time-dependent trajectory regression algorithms such as the one discussed in~\cite{DBLP:conf/aaai/ZhengN13}.

\subsection{Map Clustering}
Searching  for shortest paths in a weighed graph is rather complex, and we choose to cluster the graph to simplify travel time estimation between any two locations.  In this paper, we devise a clustering algorithm to split the road network $G(N,E)$ into separate landmark regions.


\emph{Definition 1. (Landmark):} A $landmark$ is a node $n \in N$, such that the number of pick-up locations near $n$ is above a threshold. Here we say two locations are near if the travel time between them is smaller than a threshold $r$.

\begin{algorithm}[t]                      % enter the algorithm environment
\caption{LandmarkGeneration}          % give the algorithm a caption
\label{algo-landmark}                           % and a label for \ref{} commands later in the document
\begin{algorithmic}[1]                    % enter the algorithmic environment
\REQUIRE A road network $G(N,E)$, historical pickup locations $H$, number of landmarks $k$, a travel time threshold $r$
\ENSURE Landmarks $L$ %\newline
\STATE $Count$[ ] $\leftarrow$ 0;
\FORALL {pickup location $h \in H$}
    \STATE Find nearest two node $n_1, n_2 \in N$  for $h$
    \STATE $Count$[$n_1$]++; $Count$[$n_2$]++;
\ENDFOR
% \STATE Get travel time between any two nodes $D$[$N$][$N$] $\leftarrow Johnson's~algorithm(G)$;
\STATE $CountFreq$[ ] $\leftarrow$ $Count$[ ];
\FORALL {node $n \in N$}
    \FORALL {$n_{n}$ near $n$}
        \STATE $CountFreq$[$n$] += $Count$[$n_{n}$];
    \ENDFOR
\ENDFOR
\STATE $Sort$($CountFreq$);
\FOR {$i=1$ \TO $k$}
    \STATE most dense node $n_{m}$ = $CountFreq.max$();
    \STATE $L.append$($n_{m}$);
    \FORALL {$n_{n}$ near $n_{m}$}
        \STATE $CountFreq$[$n_{n}$] -= $Count$[$n_{m}$];
    \ENDFOR
    \emph{SortUpdate}($CountFreq$);
\ENDFOR
\end{algorithmic}
% \vspace{-2em}
\end{algorithm}


We use Algo.~\ref{algo-landmark} to generate $k$ landmarks from past pick-up locations. The main reason to cluster the graph by landmarks is that we can easily find a nearby reference location for pick-up locations, and reduce the expected estimation error. We form a cluster for each landmark, and merge other nodes to the cluster with shortest travel time to the corresponding landmark, as explained in Algo.~\ref{algo-clustering}. Fig.~\ref{cluster_illustration} shows a result of clustering a city area, where each color represents a cluster. We associate an estimation error $C_i.err$ for each cluster $C_i$, which is the longest travel time between $C_i$'s landmark and any node in $C_i$.

One challenge in this process stems from the fact that cluster regions have irregular shapes. Given a lat/lon coordinate, it's sometimes hard to tell which region it falls in. To address this challenge, we partition the city area into small grids (100x100 $m^2$ in our experiment), and map each grid to the cluster that cover most of the grid area. As such, given a lat/lon coordinate, we first map the location to a grid and then map the grid to a cluster. This process runs in $O(1)$ time.

We also precompute the shortest travel time (denoted by $T_{ij}$) between each landmark pair $C_i.m$ and $C_j.m$. The results are saved in a distance matrix $T$. Now imagine that each geographical point collapses to the nearest landmark. Given two nodes in clusters $C_i$ and $C_j$, the estimated travel time $t$ is bound by Equ. \ref{estimatedtraveltime}:
\begin{equation}
\label{estimatedtraveltime}
|t - T_{ij}| \leq C_i.err + C_j.err.
\end{equation}
Using this novel travel time estimation approach, we avoid the expensive shortest path calculation.% ***YZ: Is this approach your innovation, or you borrowed the idea from somewhere? We need to clearly state that. ****

\begin{algorithm}[b]                      % enter the algorithm environment
\caption{RoadNetworkClustering}          % give the algorithm a caption
\label{algo-clustering}                           % and a label for \ref{} commands later in the document
\begin{algorithmic}[1]                    % enter the algorithmic environment
\REQUIRE A road network $G(N,E)$, landmarks $L$
\ENSURE Road network clusters $C$%\newline
\FORALL {$n \in N$}
    \STATE Find the closest landmark $C_i.m$ for $n$.
    \STATE Mark the closest distance as $d_{min}$
    \STATE $C_i.append$($n$);
    \IF {$d_{min} > C_i.err$}
        \STATE $C_i.err$ = $d_{min}$;
    \ENDIF
\ENDFOR

\end{algorithmic}
\end{algorithm}


\begin{figure}[t]

\centering
\includegraphics[width=0.4\textwidth]{fig/example_cluster}

        % \vspace{-1.8em}
\caption{Map decomposition and clustering of an area with 3 requests}%\label{example}
\label{cluster_illustration}%}
\end{figure}