\section{Online processing}
In this section, we first give a formal definition of the taxi-sharing problem,  which is NP-hard. We then propose an optimal algorithm that is practical for a small number of requests. When the number of requests becomes large, we propose a two-phase heuristic algorithm.

\subsection{Problem Formulation}
We use $\mathcal{R}=\{R_1,\cdots,R_n\}$ to denote the set of requests at present, and $\mathcal{V}=\{V_1, \cdots, V_m\}$ the set of on-road taxis. We define the real-time taxi-sharing problem in a complete directed graph $G=(N,A)$, where $N=O\cup P \cup D$, $O=\{1, \cdots, m\}$, $P=\{m+1, \cdots, m+n\}$ and $D= \{m+n+1, \cdots, m+2n\}$. $P$ and $D$ denote pick-up locations $\{R_1.p, \cdots, R_n.p\}$ and drop-off locations $\{R_1.d,\cdots,R_n.d\}$, while $O$ represents the current locations of $m$ taxis $\{V_1.l, \cdots, V_m.l\}$.

We assume each taxi has the same capacity $c$ (i.e., number of seats). Each vertex $i \in N$ is associated with a load $q_i$, such that $q_i \geq 0$ for $i \in O \cup P$ and $q_i = -q_{i+n}$ for $i \in P$. We use load to represent the number of passengers of a request. The load at a pick-up location is positive, while the load at a drop-off location is negative. A time window $[e_i, l_i]$ is associated with node $i \in P \cup D$, where $e_i$ and $l_i$ represents the earliest and latest time, at which service may begin at node $i$. For each vertex $i \in D$, we use $\beta_i$ to denote the tip parameter $\beta$ of the corresponding request $R_{i-m-n}$. Each arc $(i,j) \in A$ is associated with an accurate travel time $t_{ij}$, which is estimated using the travel time estimation algorithm discussed in~\cite{he2012mice, wang2014travel}.

For each arc $(i,j) \in A$ and each vehicle $k\in \mathcal{V}$, let $x^k_{ij} = 1$ if vehicle $k$ is scheduled to travel from node $i$ to node $j$; otherwise $x^k_{ij}$ is $0$. For each node $i \in N$ and each vehicle $k \in \mathcal{V}$, let $T^k_i$ be the time when $k$ arrives at node $i$, and $Q^k_i$ be the load of vehicle $k$ after visiting node $i$.
%At start, $B^k_k = 0$ and $Q^k_k=0$.

This problem is a generalization of the Traveling Salesman Problem with Time Window, which has already been proved to be NP-complete. So our taxi-sharing problem is NP-hard. We thus omit the detailed proof for the sake of space.

\subsection{Optimal Solution}
We solve the optimal solution for the above formulation through Integer Linear Programming (ILP).

Next, we introduce the constraints of ILP:

\begin{itemize}
\item  The flow out of the original location of each taxi is at most one:
\begin{equation}
    \label{1.1} \sum_{j\in N} x^k_{k,j} \leq 1  ~~~~~~\forall k \in \mathcal{V}.
\end{equation}

\item  All the flows entering a pick-up location will eventually leave the location (Equ. \ref{1.2}):
\begin{equation}
    \label{1.2} \sum_{j\in N} x^k_{ji} = \sum_{j\in N} x^k_{ij} ~~~~~~\forall i \in P, k\in \mathcal{V}.
\end{equation}

\item  The flow of all taxis out of a pick-up location is $1$ (Equ. \ref{1.3}), which means that only one taxi is scheduled to serve the request. The same taxi also traverses the corresponding drop-off location (Equ. \ref{1.4}).
\begin{eqnarray}
    \label{1.3} \sum_{k\in \mathcal{V}} \sum_{j\in N} x^k_{ij} = 1&~&\forall i \in P .\\
    \label{1.4} \sum_{j\in N} x^k_{ij} = \sum_{j\in N} x^k_{j,i+n} &~&\forall i \in P, k \in \mathcal{V}.
\end{eqnarray}

\item  The flow entering a destination is larger than the departing flow:
\begin{equation}
    \label{1.5} \sum_{j\in N} x^k_{ji} \geq \sum_{j\in N} x^k_{ij} ~~~~~~\forall i \in D, k\in \mathcal{V}.
\end{equation}

\item The consistency of arrival time at nodes is constrained by Equ. \ref{1.6}. The arrival time at each node is in the required interval (Equ. \ref{1.7}), and the arrival time at the pick-up node is earlier than the corresponding drop-off node (Equ. \ref{1.8}).
\begin{eqnarray}
\label{1.6} T^k_j \geq (T^k_i + t_{ij})x^k_{ij} &~&\forall i\in N, j\in N, k\in \mathcal{V}. \\
\label{1.7} e_i \leq T^k_i \leq l_i &~&\forall i \in P \cup D, k\in \mathcal{V}. \\
\label{1.8} T^k_i \leq T^k_{n+i} &~&\forall i \in P, k\in \mathcal{V}.
\end{eqnarray}

\item We constrain the number passengers on the taxi (Equ. \ref{1.9}), which is not to exceed the taxi capacity (Equ. \ref{1.10}).
\begin{eqnarray}
\label{1.9} Q^k_j = (Q^k_i + q_j)x^k_{ij} &~&\forall i \in N, j\in N, k\in \mathcal{V}. \\
\label{1.10} Q^k_i \leq c &~&\forall i \in N, k\in \mathcal{V}.
\end{eqnarray}
\end{itemize}

Equ. \ref{1.6} and \ref{1.9} are not linear. Inspired by~\cite{cordeau2006branch}, we introduce constants $M^k_{ij}$ and $W^k_{ij}$ such that these constraints can be made linear:
\begin{equation}
T^k_j \geq T^k_i + t_{ij} - M^k_{ij}(1-x^k_{ij}) ~ \forall i\in V, j\in V, k\in K,
\end{equation}
\begin{equation}
Q^k_j = Q^k_i + q_j - W^k_{ij}(1-x^k_{ij}) ~ \forall i \in V, j\in V, k\in K.
\end{equation}
These constraints are validated by setting $M^k_{ij} \geq max\{0,l_i + t_{ij} -e_j\}$ and $W^k_{ij} \geq min\{c, c+q_i\}$

In the taxi-sharing problem, we have two objective functions. The first one is to maximize the profit of the drivers. The profit consists of basic taxi fare and the tips. As discussed in Sec.~\ref{basictaxifare}, the basic taxi fare is same when the request is served by different drivers or with different traveling time. Therefore, we only need to maximize the tips:
\begin{equation}
\max \sum_{i\in D}\Big(\alpha_i - \beta_i(\sum_k T_i^k - e_i)\Big).
\end{equation}
The second objective is to maximize the QoS of passengers, which is quantified as the sum of weighted delay time of passengers:
\begin{equation}
\label{WeightedQOS}
% \min \sum_{i\in D}\big(\frac{T_i}{l_i-e_i}\sum_{k\in \mathcal{V}}B_i^k\big)
\min \sum_{i\in D}\beta_i\sum_{k\in \mathcal{V}}T_i^k.
\end{equation}
As $\alpha_i$ and $e_i$ are constant in the problem model, these two objective functions are consistent with each other. In other words, we could maximize the profit of drivers and the QoS of passengers at the same time. For simplicity, we use profit to quantify the QoS in the following part of this paper.  %***YZ: consistent with what?? ***
% subject to
% \begin{eqnarray}
    % \label{1.2} \sum_{k\in \mathcal{V}} \sum_{j\in N, j\neq i} x^k_{ij} = 1&~&\forall i \in P \\
    % \label{1.3} \sum_{j\in N} x^k_{ij} = \sum_{j\in N} x^k_{j,i+n} &~&\forall i \in P, k \in \mathcal{V} \\
    % \label{1.4} \sum_{j\in N} x^k_{ji} = \sum_{j\in N} x^k_{ij} &~&\forall i \in P, k\in \mathcal{V} \\
    % \label{1.5} \sum_{j\in N} x^k_{ji} \geq \sum_{j\in N} x^k_{ij} &~&\forall i \in D, k\in \mathcal{V} \\
    % \label{1.6} \sum_{j\in N} x^k_{k,j} \leq 1  &~&\forall k \in \mathcal{V} \\
    % \label{1.7} T^k_j \geq (T^k_i + t_{ij})x^k_{ij} &~&\forall i\in N, j\in N, k\in \mathcal{V} \\
    % \label{1.8} Q^k_j = (Q^k_i + q_j)x^k_{ij} &~&\forall i \in N, j\in N, k\in \mathcal{V}\\
    % \label{1.9} e_i \leq T^k_i \leq l_i &~&\forall i \in P \cup D, k\in \mathcal{V} \\
    % \label{1.10} T^k_i \leq T^k_{n+i} &~&\forall i \in P, k\in \mathcal{V}\\
%     \label{1.11} Q^k_i \leq c &~&\forall i \in N, k\in \mathcal{V}\\
%     \label{1.12} x^k_{ij} \in \{0,1\} &~& \forall i \in N, j\in N, k\in \mathcal{V}.
% \end{eqnarray}

% which is to minimize QoS from the passengers' side; meanwhile maximize the profit from the drivers' side. Equ. \ref{1.2} and \ref{1.3} constraints that each request is served exactly once, and the same taxi would visit the pick-up and destination node of a request. Equ. \ref{1.4} ensures that a taxi would always leave a pick-up location. Similarly equ. \ref{1.5} constraints that the flow out of a destination location is smaller than the flow into it. Equ. \ref{1.6} ensures that the flow out of the origin location of each taxi is at most one. Consistency of the arrival time and load variables is defined by equ. \ref{1.7} and \ref{1.8}. Equ. \ref{1.9} and \ref{1.10} ensures that the service time is between the time windows and the arrival time of a destination location is after the one of a pick-up location. Equ. \ref{1.11} imposes capacity constraints.



% The vehicle routing problem is NP-hard, and thus, our taxi-sharing problem is also NP-hard.
When we have a small number of requests and taxis, we can solve this ILP formulation with a branch and cut algorithm.

\subsection{Heuristic Taxi-Sharing Algorithm}

Our dispatching system aims to respond to requests within a short delay. Solving the proposed ILP problem is too time-consuming when we have a large number of requests, thus rendering the system unable to serve the requests on the fly. In this case, instead of obtaining the global optimal schedule, we propose a heuristic approach that reduces the problem space by focusing on the taxis in the requested region. In order to locate taxis in a certain location, we need to build efficient index structures to facilitate location-based taxi search.

% we first introduction a novel map decomposition method and split the city area into landmark regions, then

\textbf{Index: }
\begin{figure}[t]
% \begin{minipage}[b]{0.5\linewidth}
\centering
\includegraphics[width=0.43\textwidth]{fig/index_example}
\caption{Index of taxis}
% \vspace{-1.5em}
\label{index_example}
\end{figure}
To facilitate fast taxi search, we build an index structure based on the pre-computed clusters. Specifically, for each cluster $C_i$, we maintain a nearby taxi list $C_i.L_t$ and  nearby cluster list $C_i.L_c$, as illustrated in Fig. \ref{index_example}. Here, $C_i.L_c$ consists of a list of nearby clusters, sorted in ascending order of the travel time from  $C_i$. This list is static, and can be pre-computed. The taxi list $C_i.L_{t}$ records the IDs of taxis that are to arrive $C_i$ in the near future, as well as the estimated arrival time. The taxis in $C_i.L_{t}$ are sorted in the ascending order of the arrival times, and this list is updated dynamically.

\textbf{Candidate Taxi Search Algorithm: }
Next we describe our taxi search algorithm, which is to find a set of candidate taxis that are suit to serve the standing requests -- candidates should be able to pick up the passenger(s) within the pick-up window, and have enough available seats to hold the passengers. Further, the candidate's existing requests should still meet their deadlines. The pseudo-code of the proposed algorithm is described in Algo.~\ref{algo-searching}. For the clarity of description, we explain the algorithm using the example illustrated in Fig.~\ref{cluster_illustration}, in which we assume the dispatching server receives three requests at the same time, namely $R_1$, $R_2$ and $R_3$. The search algorithm checks each request individually. Taking $R_1$ for example,
we first find the resident cluster $C_1$, where $R_1.p$ is located. Next we search for candidate clusters. A cluster $C_i$ is considered a \emph{candidate cluster} if taxis located in $C_i$ can arrive at cluster $C_1$ within the pick-up window:
\begin{equation}
\label{candidate clusters}
t_{curr} + T_{1i} - C_1.err - C_i.err \leq R_1.pw.l,
\end{equation}
where $t_{curr}$ is the current time. The algorithm checks all clusters in the ordered list $C.t$, and breaks when the next cluster fails to satisfy Eqn~\ref{candidate clusters}. For the example in Fig.~\ref{cluster_illustration}, clusters $C_2, C_6, C_7$ are candidate clusters.

The algorithm next examines each candidate cluster $C_i$ to find out whether there are taxis that satisfy the following two conditions: 1) the arrival time at $C_i$ is no later than $R_1.pw.l - T_{1i} + C_1.err + C_i.err$; and 2) the taxi has enough empty seats when arriving in cluster $C_i$.  As such, we can locate a list of candidate taxis for each request.

\begin{algorithm}[b]                      % enter the algorithm environment
\caption{Candidate Taxi Searching Algorithm}          % give the algorithm a caption
\label{algo-searching}                           % and a label for \ref{} commands later in the document
\begin{algorithmic}[1]                    % enter the algorithmic environment
\REQUIRE A road network $G(N,E)$, indexes of taxis $C.L_c$ and $C.L_t$, requests $\mathcal{R}$, and the distance matrix $T$.
\ENSURE Candidate taxis to serve each request $R$%\newline
\FORALL {$R \in \mathcal{R}$}
    \STATE Find resident cluster $C_i$ for $R.p$;
    \FORALL {taxi $V \in C_i.L_t$}
        \IF {$V$ arrive $C_i$ before request deadline $R.pw.l$ \\
            \AND $V.n > R.n$}
            \STATE Mark $V$ as candidate for $R$;
        \ENDIF
    \ENDFOR
    \FORALL {$C_j \in C_i.L_c$}
        \IF {$T_{ij} > R.pw.l$}
            \STATE Break;
        \ENDIF
        \FORALL {$V \in C_j.L_t$}
            \IF {$t_{curr} + T_{ij} - C_i.err - C_j.err < R.pw.l$ \\
                \AND $V.n > R.n$}
                \STATE Mark $V$ as candidate for $R$;
            \ENDIF
        \ENDFOR
    \ENDFOR
\ENDFOR
\end{algorithmic}
\end{algorithm}

\textbf{QA-Share Scheduling: }
After identifying candidates for each request, the scheduling algorithm next calculates the schedule for these taxis, with the objective of maximizing drivers' profit and passengers' QoS at the same time. %***YZ: how about QoS?? ***

For each request, as in \cite{DBLP:conf/icde/MaZW13}, we assume that the order of pick-up/drop-off locations in the existing schedule remain unchanged while new locations are inserted into the schedule. That is, the scheduling algorithm tries to insert the new request's pick-up and drop-off locations into the taxi's delivery schedule while satisfying the following conditions: 1) the requesting passenger(s) can be picked up and delivered before the specified deadlines; 2) there are enough seats to take the requesting passengers at the pick-up location; and 3) accepting the request increases the profit of the taxi driver. After examining these conditions with each candidate taxi, we pick the one that gives the most profit.

% We maintain a list of maximum delay $V.D$ between any two points in the current schedule. When a new point $i$ is added to the schedule between $j$ and $j+1$, all the maximum delays of intervals after the point should be updated:
% \begin{equation}
% d = d + (T(j,i)+T(i,j+1)-T(j,j+1)),
% \end{equation}
% while all the interval between $j$ and $i$ should be set as:
% \begin{equation}
% d = min(d_{j,j+1},~R.dt-arrival~time~at~i)
% \end{equation}
% With the help of the list, we could check, whether the time delay incurred by inserting $R.p$ and $R.d$ leads to failure in delivery of other passengers.

% The scheduling algorithm is as shown in Algo \ref{algo-scheduling}. We check each candidate insertion, whether it will lead to failure in deliver other passengers, and whether it can server the request before deadline. If it's a possible insertion, we would try to calculate the incurred profit with the pricing scheme, with the travel time matrix among every two cluster, and mapping locations to clusters for estimation.

It may so happen that a single taxi can be chosen by multiple requests (that are submitted within a short time frame $PW$), in which we say a scheduling conflict has occurred. A conflict can be potentially addressed in different ways. In the simplest case, we may be able to schedule the candidate taxi to serve all the requests. If that is impossible, we keep a subset of requests to the chosen taxi, and schedule the rest of requests to the other qualified taxis. We achieve this objective by solving a weighted max bipartite matching problem. In the bipartite graph,  one set of nodes are all the requests, while the other set consists of  the candidate taxis. An edge between taxi $t$ and  request $r$ means $t$ can serve $r$, where the weight of the edge is the profit if $t$ serves $r$. We can solve the maximum weighted bipartite matching problem optimally with \emph{Hopcroft-Karp} algorithm~\cite{hopcroft1973n} with $O(\sqrt{N}E)$ complexity. When we have a large number of nodes, but a small number of edges, this algorithm runs very fast.

%This algorithm  as the number of nodes and edges are normally small. For the third kind of solutions, simply combining the above two algorithm works. We then compare the profit of all possible solutions, and output the schedule with most profit.

% The taxis in $C_i.L_{u}$ are sorted in the ascending order of urgency, which is measured by summing up the tip parameter $\beta$ indexes proposed by on-board passengers. The bigger $\sum_i \beta_i$ is, the more urgent on-board passengers are.

% \begin{figure}[!t]
% \centering
% \includegraphics[width=0.7\linewidth]{fig/update_mechanism}
% \caption{Update Mechanism}
% \label{UpdateMechanism}
% \end{figure}


% \begin{algorithm}%[!ht]
% \caption{Scheduling Algorithm}
% \label{algo-scheduling}
% \begin{algorithmic}[1]
% \REQUIRE A collection of requests $R$, a set of candidate taxis $\{r.V\}$ for each request $r$, a threshold $k$ and a pricing scheme.
% \ENSURE Taxi $r.v$ for each request $r$  %\newline
% \FORALL {$r \in R$}
%     \FORALL {$candidateTaxi \in r.V$}
%         \STATE $S \leftarrow candidataTaxi.S$
%         \FORALL {$i \in S$}
%             \STATE $pDelay = tvlTime(i, r.p)+tvlTime(r.p, i+1)$
%             \IF {$pDelay < maxDelay(i,i+1)$}
%                 \FOR {$j=1$ \TO $n$}
%                     \STATE $dDelay = tvlTime(j, r.d)+tvlTime(r.d, j+1)$
%                     \IF {$dTime+pTime < maxDelay(j,j+1)$
%                         \AND arrival time before deadline}
%                         \STATE $pro = profit(S_{new})$
%                         \IF {$pro > candidateTaxi.pro$}
%                             \STATE $candidateTaxi.pro = pro$;
%                             \STATE $candidateTaxi.S = S_{new}$;
%                         \ENDIF
%                     \ENDIF
%                 \ENDFOR
%             \ENDIF
%         \ENDFOR
%     \ENDFOR
% \ENDFOR
% \FORALL {$r \in R$}
%     \STATE Dispatch a taxi with max profit
% \ENDFOR
% \FORALL {$taxi$ chosen by a set of requests $R$}
%     \STATE create a weighted bipartite graph $G'(A\cup B, E')$, where $R$ is modeled by nodes in $A$, top-$k$ profitable taxis for $R$ is modeled by nodes in $B$, the weight of the edges is equal to the profit for a taxi to serve a request;
%     \STATE $max~matching$ = $HOPCROFT-KARP(G')$;
%     \STATE $p_1 = profit(max~matching)$;
%     \STATE $p_2 = profit(taxi~serving~several~requests$;
%     \STATE Dispatch the one with maximum profit;
% \ENDFOR
% \end{algorithmic}
% \end{algorithm}

\subsection{Dynamic Schedule Update}
\label{updatemechanismsection}
As discussed in Sec.~\ref{introduction}, when more requests are submitted, the dispatching system may need to recalculate the schedules. In QA-Share, we adopt the following update mechanism: we periodically re-calculate the schedule using the above method, considering both new requests and those requests that are scheduled but not served. Then we update the schedule if it leads to an increased profit.

With periodic scheduling updates, one may fear that oscillation might occur -- a taxi may be requested to drive towards a different direction after each update, leading to no real progress. In order to mitigate oscillation,  we propose to update the schedule only when the improved profit is above a threshold $\Phi$. A large $\Phi$ value can effectively mitigate the oscillation likelihood. Indeed, as shown in Sec.~\ref{evaluation}, we did not notice much oscillation with $\Phi = 2$ dollar/request.

% ***YZ: my feeling is that we focus more on profit than QoS. The example shown in Introduction seems irrelevant to the algorithm. ****