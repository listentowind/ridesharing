\section{Introduction}
%background
\label{introduction}
 In modern cities, taxis are playing an increasingly important role in supporting people's commute. According to a survey conducted in New York city~\cite{newyork}, over 100 taxi companies operate more than 13,000 taxis in New York, delivering 660,000 passengers every day. They convey  more than 25$\%$ of the passengers, accounting for 45$\%$ of total transit fares. Though important, today's taxi system is far from being efficient: a common problem is that  people often have difficulties in hailing a taxi during rush hours, while occupied taxis may still have available seats. This inefficiency becomes much worse in countries and areas that are still in the process of urbanization, where the city population increases at a faster rate than the number of taxis.

This problem has attracted some attention in recent years, resulting in a proposed solution called ride-sharing, in which people share a taxi with others who have similar itineraries and schedules. %Specifically, a taxi-sharing system dispatches taxis to serve real-time requests  which include users‘ pickup and dropoff locations, as well as time constraints.
When a new passenger request arrives, the taxi-sharing server will try to dispatch a taxi to satisfy the new request while serving its existing requests (if any). Taxi-sharing has to track passenger requests and taxi locations on a real-time basis, both of which are highly dynamic and hard to predict, thus posing great challenges on the underlying system.

Several taxi-sharing schemes have been proposed in the literature, such as those discussed in~\cite{DBLP:conf/fskd/TaoC07,DBLP:conf/vtc/ChenLC10,DBLP:conf/sensys/ZhangLZLLH13, DBLP:conf/icde/MaZW13}. However, they all fall short of the requirements of a scalable taxi-sharing system that serves real-time user requests. For example,  CoRide~\cite{DBLP:conf/sensys/ZhangLZLLH13} focuses on static scenarios in which passengers and taxis are at the same pickup location (e.g., an airport). When a new request arrives, CoRide simply inserts the request to the end of an occupied taxi's schedule, or arranges an empty taxi.  On the other hand, T-Share~\cite{DBLP:conf/icde/MaZW13} does consider dynamic scenarios, but its scheduling is rather simplistic: for each new request, it simply assigns the taxi with the minimum additional driving distance, without considering the Quality of Service (QoS) for the passengers on the chosen taxi. Once a sharing schedule is made, it remains unchanged even though a new schedule may be more efficient.

%In other words, T-Share considers local optimal of only one request.

\begin{figure}[t]
% \vspace{0.5em}
\subfigure[Request $R_1 (P \rightarrow O) $ is received. Taxi B is better to serve the request.]{
\label{qos_example}
\includegraphics[width=.45\linewidth]{fig/exp1}}
\hspace{0.01cm}
\subfigure[Then, request $R_2 (Q \rightarrow O)$ is received. It's better to update the current schedule.]{
\label{update_example}
\includegraphics[width=.45\linewidth]{fig/exp2}}
% \vspace{-0.5em}
\caption{Example to show the two challenges}
% \vspace{-1.8em}
\end{figure}

% \begin{figure}[!t]
% \centering
% \includegraphics[width=0.7\linewidth]{fig/firstExample}
% \caption{Both taxi A and B can serve request $R_1$ from pickup location $R_1.p$ to destination $R_1.d$. We believe taxi B is a better choice, because service quality of only one passenger is influenced.}
% \label{example1}
% \end{figure}

% Online requests are also mentioned. CoRide will either simply add online requests to the end of an existing route of a taxi, or allocate a free taxi for this new passenger.

% propose a taxi searching algorithm using a spatial-temporal index to quickly retrieve candidate taxis. They then check each candidate, and insert the request into the schedule of the taxi with minimum additional incurred travel distance.

In this study, we aim to improve the state-of-the-art taxi-sharing strategy by addressing two important challenges. The first challenge we target at is to find balance between enhancing QoS for passengers (e.g. travel time, number of strangers that share a taxi, etc.) and maximizing profit for taxi drivers. As an example, in traditional non-sharing taxi services, greedy drivers may choose a longer route to increase his/her own profit, extending the passenger's travel time.  In a sharing system, if not careful, passenger's QoS will be hurt even more.  People choose to take taxis for a comfortable, door-to-door service. By sharing a taxi with strangers, passengers may not only feel a loss of privacy, but also have to take a longer route in many cases, which may severely undermine their motivation for participating in taxi-sharing.
Fig.~\ref{qos_example} illustrates our up-mentioned point. As shown a request $R_1$ arrives with pickup location $P$ and destination location $O$. The dashed lines represent the planned routes of taxi $A$ and $B$.  Both CoRide~\cite{DBLP:conf/sensys/ZhangLZLLH13} and T-Share~\cite{DBLP:conf/icde/MaZW13} choose taxi $A$ to serve the request, because of its shorter detour distance. Clearly, this selection does not take into consideration passengers' QoS preferences -- it affects three existing passengers on taxi $A$, whereas the selection of taxi $B$ will only affect one existing passenger.

Another challenge we target at is the dynamic nature of taxi-sharing, due to dynamically arriving requests.  For example,  in Fig.~\ref{update_example}, after scheduling Taxi $B$ to serve request $R_1$, let us assume that the dispatching system receives an urgent request $R_2$ from pickup location $Q$ to the destination $O$. In this case, taxi $A$ is too far away to serve $R_2$, and taxi $B$ is going to be too late for $R_2$ if it has to serve $R_1$ first.  Therefore, a better choice is to dynamically update the schedule such that taxi $B$ directly goes to serve $R_2$, while taxi $A$ goes to serve $R_1$. Dynamically updating schedules is very challenging problem, because one needs to deal with a large number of requests, and needs to avoid such situations that taxis keep reversing directions with dynamic updates of schedules.

To address these challenges, in this work, we propose an efficient and scalable QoS-Aware dispatching approach for taxi-sharing, referred to as \emph{QA-Share}. In this paper, QoS refers to the waiting time for a taxi, the traveling time to the destination, and the number of strangers to share the taxi. We propose a taxi-sharing model such that the QoS of passengers and the profit of drivers are maximized at the same time. We formally define the taxi-sharing dispatching problem, which is shown to be NP-hard. We then provide optimal dispatching solutions with integer linear programming for a small number of requests. When the number of requests becomes sufficiently large, we propose a heuristic online algorithm to speed up the scheduling process. We design a map clustering algorithm and build indexes of taxi locations. Based on the indexes, a fast taxi searching and scheduling algorithm is then discussed. We also design a dynamic update mechanism to re-schedule the taxi-sharing on the fly as new requests arrive.

We conduct trace-driven simulation with realistic dataset in a Chinese city -- ZhenJiang -- which contains the GPS traces recored by over 3,000 taxis during a period of three months in 2013. The detailed evaluation results show that compared with earlier work, the QoS and profit is increased by 16\%. Compared with the ground truth, the fraction of requests that get served is increased by up to 45\%.  To sum up, the key contributions of this work are three-fold:
\begin {itemize}
\item To the best of our knowledge, this is the first study that proposes a taxi-sharing service that maximizes the QoS of passengers as well as the profit of drivers;% To the best of our knowledge, we are the first to propose a taxi-sharing model and dispatching algorithms, such that the QoS of the passengers and the profit of drivers are maximized at the same time.
\item We give optimal dispatching solutions by integer linear programming. We also propose heuristic algorithms to speed up the process, and dynamic update mechanism to further maximize profit;

\item This is the first study that conducts simulations with traces from a city with population of 3 million, showing that taxi-sharing is viable in a medium city.

\end{itemize}


The rest of the paper is organized as follows. Sections II introduces some related work. Section III presents the system overview. Section IV describes the pre-processing stage including map decomposition and clustering. Section V explains the algorithms for taxi scheduling. Section VI presents our update mechanism. Finally Section VII validates our system, followed by the conclusion in Section VIII.
% The contribution of this paper is multiple-dimensional:


% \section{Motivation}
% In this section, based on the taxi traces of 1,000 taxis in ZhenJiang, we first introduce some basic properties of taxi services in developing countries. We then present some evidences about the inefficiency of the taxi services, demonstrate the benefits for taxi-sharing and analyze the passenger sensitivity of service quality. We identify challenges of a real-time and large-scale taxi-sharing system.

% \subsection{Properties of taxi services}
% In developed countries, almost every adult owns a private car for daily commute. Taxis usually serve passengers going to airports or railway stations. However in developing countries, taxis and other public transportation are popular for daily activities, since it's too expensive to drive a private car. The increasing pursuit of life quality leads to a large taxi market in dense urban areas in developing countries such as ZhenJiang. We focus on such a large-scale taxi market, where requests would have different arbitrary pickup and delivery locations.

% According to Chen et al. \cite{chen2010fuel} and Tao et al. \cite{tao2007dynamic}, people usually don't book a taxi hours before. Thus requests would be dynamic, such that it may appear to the dispatching system in real time, and the dispatching system should be able to adjust schedule of a proper taxi dynamically.

% \subsection{Inefficiencies of taxi services}
% It is increasingly difficult to take a taxi during rush hours, yet the occupancy rate is low, delivery intervals are long and delivery distance is short. $(problem) Could we generate occupancy rate from our dataset? $
% \begin{figure}[t]
%         \centering
%         \subfigure[Occupancy Rate]{
%                 \label{fig:occupancy_rate}
%                 \includegraphics[width=0.14\textwidth]{fig/occupancy_rate}}
%                 % \caption{Occupancy Rate}
%         \hfill
%         \subfigure[Delivery Intervals]{
%                 \includegraphics[width=0.14\textwidth]{fig/delivery_intervals}
%                 \label{fig:delivery intervals}}
%         \hfill
%         \subfigure[Delivery Distance]{
%                 \includegraphics[width=0.14\textwidth]{fig/delivery_distance}
%                 \label{fig:delivery_distance}}

%         \caption{Inefficiency Analyze of Existing Taxi Services}\label{fig:inefficiency}
% \end{figure}
% Based on the taxi traces in ZhenJiang, we analyze the inefficiencies among existing taxi system in Fig. \ref{fig:inefficiency}. Fig. \ref{fig:occupancy_rate} shows the overall seats occupancy ratios. We can see that even in rush hours, the occupancy rate is also low, since many taxis only take one passenger. Many potential passengers failed to hail a taxi. Fig. \ref{fig:delivery intervals}

% \subsection{Benefits for taxi-sharing}
% From the point of view of the passengers, taxi-sharing combines the advantages of the traditional taxi service (comfortable, door-to-door transportation, flexible) with the ones of public transportation (e.g. reduced cost, social communication). From the point of view of the taxi drivers, taxi-sharing helps to serve more passengers and earn more money even thought they charge less on each single passenger. As for the society and environment, taxi-sharing would relieve traffic congestions, save energy consumption and reducing air pollution. The flourish of advantages has sharpen the urge for large-scale, robust and off-the-shelf taxi-sharing system. Actually, in China, ad-hoc taxi-sharing has a long history, as it brings profit to the drivers. Some drivers would frequently stop and ask on road passengers whether they would share a taxi or not. A taxi-sharing system can solve these unnecessary and tedious queries.

% \subsection{Passenger sensitivity for service quality}
% According to a taxi-sharing survey taking at Beijing of 274 interviewees \cite{data100}, the key concerns about taxi-sharing is i) the prolonged travel time (64$\%$), ii) hard to find passengers to carpool (50$\%$), and iii) unable to print duplicated receipts for all passengers (50$\%$).

% [Find any survey about the relation between tip and delayed time (failed to find one for now)]

% \subsection{Challenges for taxi-sharing}
% We present three challenges for implementing a large-scale taxi-sharing service in the current taxi networks.

% \textbf{Tradeoff between Service Quality and Profit}: People choose to transport by taxi, for a comfortable, door-to-door service. They will however, not only get an uncomfortable feeling, but also make a detour to pickup or deliver other passengers, when choose sharing a taxi with strangers. The service quality will be further harmed, when selfish taxi drivers just aim to earn more money, without considering the riding time of passengers. High profit with low service quality will weaken passengers' motivation for ride-sharing. Thus we need to provide a taxi-sharing service, which would optimize both service quality and profit.

% \textbf{Real Time Response for Large-scale Online Requests}: Thousands of taxis are driving in the city area day and night, and they will upload detailed information to a centralized dispatching system. For example, locations and how many seats are available. On the other hand, a large number of requests should be serviced in real time. A centralized dispatching system should search a suitable taxi from a large dataset of taxis, and schedule a route for the taxi in a big graph (i.e. a city map).

% \textbf{Pricing Scheme}: The taxi-sharing service should notify passengers with the fare details, when they arrive at the destination. In order to motivate taxi-sharing, a reasonable pricing scheme should have the following properties: 1) an individual passenger would pay less money, compared with taking a taxi alone; 2) on-taxi passengers would pay further less, when more passengers get aboard; 3) drivers will earn more money than driving without ride-sharing; 4) drivers will earn further more, when taking more taxi-sharing passengers. What's more, how to deal with the situation, where several friends want to take a taxi together. Should we charge them separately or as a whole?

% To address the above challenges, we aim to develop a on-line taxi-sharing system, which aim to optimize service quality and profit at the same time, with a reasonable on-line pricing scheme.

% [unsolved challenges:

% 1. Uncomfortable with strangers on the taxi

% 2. Security problems. What if a passenger lost some stuff? Who should be responsible?

% 3. Refuse serving. Some drivers may choose to avoid picking up a request with several passengers or a request which refuses ride-sharing.

% 4. Pricing. How to price a family and a single passenger. Device to provide separated tickets.
% ]